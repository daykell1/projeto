# Projeto Sprint-1 


# Descrição 
Esse repositório foi criado com intuito de documentar  os estudos que estão sendo feitos para apresentar no final dessa sprint, para o (Programa de Bolsas da Compass UOL - AWS for Software Quality & Test Automation.)


# objetivo

* Comunicação em projetos
* Como criar bons READMEs para repositórios versionados
* Matriz de Eisenhower
* Fundamentos de agilidade
* Organização em equipes
* O QA Ágil
* Fundamentos de Teste (back-end)
* Pirâmide de testes
* Myers e o princípio de Pareto
* Fundamentos de CyberSecurity

# Avaliação

* Apresentação Exercícios & Versionamento
* Apresentação breve da resolução dos exercícios propostos na Sprint através do repositório com o versionamento no Gitlab.

* Review Sprint 1

* O que precisa mudar?
* Quais conteúdos teve mais dificuldade?
* o que Funcionou?
